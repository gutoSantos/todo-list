# Backend

## Install

Para instalar as dependências da aplicação, é necessário executar o comando `npm install`. Com isso todas as dependencias necessárias serão instaladas.

## Development server

Execute o comando `npm start` para subir a aplicação. 
A aplicação estará disponivel no endereço `http://localhost:3000/`.


## DB

O banco de dados está hospedado no site `https://remotemysql.com/phpmyadmin/index.php?db=s0EdMMWRXI&table=TodoItens&target=tbl_sql.php`. Os dados de acesso se encontram no arquivo `/backend/src/config/pool.factory.js`.