// Cria um middleware para fornecer
// uma nova conexão sempre que houver
// uma requisição. Fica responsável por atribuir
// essa nova conexão a requisição
// para que assim toda a cadeixa possua uma conexão válida
module.exports = pool => (req, res, next) => {

    // Solicita a conexão ao Pool
    pool.getConnection((error, connection) => {

        // Caso de alguma exceção
        // É repassado para o middleware responsável
        if (error) {
            return next(error);
        }

        console.log('pool => obteve conexão');

        // Adiciona a conexão ã requisição
        req.connection = connection;

        // prossegue o fluxo da requisção
        next();

        // Quando o response for enviado
        // é feita a liberação da conexão
        res.on('finish', () => req.connection.release());
    });

};