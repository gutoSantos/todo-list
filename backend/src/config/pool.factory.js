// Factory responsável por administrar 
// as conexões com o DB

const mysql = require('mysql');

// Cria a conexão com o DB
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'remotemysql.com',
    port: 3306,
    database: 's0EdMMWRXI',
    user: 's0EdMMWRXI',
    password: 'IZwirped5J',

    // Realiza o cast do tipo BIT
    // Pois no mysql o tipo BIT vira Buffer
    typeCast: (field, next) => {
        if ((field.type !== "BIT") || (field.length !== 1))
            return next();

        const bytes = field.buffer();
        return (bytes[0] === 1);
    }
});

console.log('pool => criado');

pool.on('release', () => console.log('pool => conexão retornada'));

// Quando a aplicação for encerrar
// Finaliza as conexões com o DB
process.on('SIGINT', () =>
    pool.end(error => {
        if (error)
            return console.log(error);

        console.log('pool => fechado');
        process.exit(0);
    })
);

module.exports = pool;