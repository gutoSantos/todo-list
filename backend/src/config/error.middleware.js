module.exports = (error, req, res, next) => {

    const msgDB = getErrorDB(error);

    console.log(error);

    if (msgDB)
        return res.status(500).json({
            error: msgDB,
            ex: error.toString()
        });

    res.status(500).json({
        error: error.toString()
    });
};


function getErrorDB(error) {

    if (!error.sqlState)
        return;

    console.log(error.code)

    switch (error.code) {
        case "ER_NO_SUCH_TABLE":
            return "Falha ao executar a operação";
        case "ER_BAD_FIELD_ERROR":
            return "Falha ao executar a operação";
        default:
            return "Falha ao acessar o banco de dados";
    }

    // Codigos existentes
    // "ER_ACCESS_DENIED_ERROR" -- Erro no Usuario, HOST, DB
    // "ER_DBACCESS_DENIED_ERROR" -- Erro na senha
    // "ER_NO_SUCH_TABLE" -- Tabela não encontrada
    // "ER_BAD_FIELD_ERROR" -- Coluna não encontrada

}