'use strict';

class TodoDao {

    // Recebe a conexão como parametro
    // Através dessa conexão que é realizado
    // o acesso ao DB
    constructor(connection) {
        this._connection = connection;
    }

    // Retorna todos os Todo`s
    getAll() {
        return new Promise((resolve, reject) => {

            const sql = "SELECT * FROM `TodoItens`";

            this._connection.query(sql, (error, result) => {
                if (error)
                    return reject(error);

                resolve(result);
            });
        });
    }

    // Adiciona um novo Todo
    add(todo) {
        return new Promise((resolve, reject) => {

            const title = todo.title;
            const done = todo.done;
            const sql = `INSERT INTO \`TodoItens\`(\`title\`, \`done\`) VALUES ('${title}', ${done})`;

            this._connection.query(sql, (error, result) => {
                if (error)
                    return reject(error);

                resolve(result);
            });
        });
    }

    // Atualiza os dados do Todo
    update(id, todo) {

        return new Promise((resolve, reject) => {

            const title = todo.title;
            const done = todo.done;
            const sql = `UPDATE \`TodoItens\` SET \`title\` = '${title}', \`done\` = ${done} WHERE \`id\` = ${id}`

            this._connection.query(sql, (error, result) => {
                if (error)
                    return reject(error);

                resolve(result);
            });
        });
    }

    // Remove um Todo
    delete(id) {
        return new Promise((resolve, reject) => {

            const sql = `DELETE FROM \`TodoItens\` WHERE \`id\` = ${id}`

            this._connection.query(sql, (error, result) => {
                if (error)
                    return reject(error);

                resolve(result);
            });
        });
    }

}

module.exports = TodoDao;