'use strict';

const TodoDAO = require('../dao/todo.dao');

exports.get = (req, res, next) => {
    new TodoDAO(req.connection)
        .getAll()
        .then(result => res.json(result))
        .catch(next);
}

exports.post = (req, res, next) => {
    new TodoDAO(req.connection)
        .add(req.body)
        .then(result => {

            // Adiciona o id no Todo
            // E retorna o mesmo para o front
            const newTodo = { ...req.body };
            newTodo.id = result.insertId;
            return res.json(newTodo);
        })
        .catch(next);
}

exports.put = (req, res, next) => {
    new TodoDAO(req.connection)
        .update(req.params.id, req.body)
        .then(result => {

            if (result.affectedRows == 0)
                res.status(400).json({ error: `Todo não encontrado na base.` });
            else
                res.json(req.body);
        }, next)
        .catch(next);
}

exports.delete = (req, res, next) => {
    new TodoDAO(req.connection)
        .delete(req.params.id)
        .then(result => {
            if (result.affectedRows == 0)
                res.status(400).json({ error: `Todo não encontrado na base.` });
            else
                res.json(req.body);
        }, next)
        .catch(next);
}