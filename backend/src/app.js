'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const pool = require('./config/pool.factory');
const connectionMiddleware = require('./config/connection.middleware.js');
const errorMiddleware = require('./config/error.middleware.js');

const app = express();
const router = express.Router();

// Carrega as Rotas
const indexRoute = require('./routes/index.route');
const todoRoute = require('./routes/todo.route');

// Apilca o middleware de conexão
app.use(connectionMiddleware(pool));

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

// Habilita o CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

// Disponibiliza as rodas para a aplicação
app.use('/api/', indexRoute);
app.use('/api/todo-list', todoRoute);

app.use(errorMiddleware);

module.exports = app;