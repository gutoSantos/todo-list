export class Todo {

    id: Number;

    constructor(public title: String, public done: Boolean) {
    }
}