import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TodoService } from "./services/todo.service";
import { ToastrService } from 'ngx-toastr';

import { Todo } from "src/model/todo.model";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
    public todos = [];
    public form: FormGroup;
    public loading: Boolean = false;

    constructor(
        private fb: FormBuilder,
        private todoService: TodoService,
        private toastr: ToastrService) {

        this.form = this.fb.group({
            title: [
                "",
                Validators.compose([
                    Validators.minLength(3),
                    Validators.maxLength(60),
                    Validators.required
                ])
            ]
        });
    }

    ngOnInit() {
        this.getAll();
    }

    showError(message) {
        setTimeout(() => {
            this.toastr.error(message);
        }, 1);
    }

    add() {

        if (!this.title.value || this.loading)
            return;

        this.loading = true;

        this.todoService
            .add(new Todo(this.title.value, false))
            .then(response => {
                this.todos.unshift(response.data);

                this.loading = false;
                this.clear();
            })
            .catch(error => {
                this.loading = false;
                this.showError("Falha ao adicionar tarefa. Tente novamente.");
                console.log(error)
            });
    }

    clear() {
        this.form.reset();
    }

    getAll() {
        this.loading = true;

        this.todoService
            .getAll()
            .then(response => {
                this.todos = response.data;
                this.loading = false;
            })
            .catch(error => {
                this.loading = false;
                this.showError("Falha ao carregar tarefas. Tente novamente.");
                console.log(error)
            });
    }

    markAsDone(todo: Todo) {

        if (this.loading)
            return;

        this.loading = true;

        // Copia o objeto para realizar a marcação no retorno
        const todoCopy = { ...todo };
        todoCopy.done = true;

        this.todoService
            .update(todoCopy)
            .then(response => {
                console.log(response)
                todo.done = true;
                this.loading = false;
            })
            .catch(error => {
                this.loading = false;

                this.showError("Falha ao registrar conclusão. Tente novamente.");
                console.log(error)
            });
    }

    markAsUndone(todo: Todo) {

        if (this.loading)
            return;

        this.loading = true;

        // Copia o objeto para realizar a marcação no retorno
        let todoCopy = { ...todo };
        todoCopy.done = false;

        this.todoService
            .update(todoCopy)
            .then(response => {
                todo.done = false;
                this.loading = false;
            })
            .catch(error => {
                this.loading = false;
                this.showError("Falha ao remover conclusão. Tente novamente.");
                console.log(error)
            });
    }

    remove(todo: Todo) {

        if (this.loading)
            return;

        this.loading = true;

        this.todoService
            .delete(todo)
            .then(response => {
                const index = this.todos.indexOf(todo);
                this.todos.splice(index, 1);
                this.loading = false;
            })
            .catch(error => {
                this.loading = false;
                this.showError("Falha ao remover tarefa. Tente novamente.");
                console.log(error)

            });
    }

    // Expondo a propriedade title do formulario
    get title() {
        return this.form.get("title");
    }
}