import { Injectable } from '@angular/core';
import axios from 'axios';
import { Todo } from '../../model/todo.model';

@Injectable({
    providedIn: 'root'
})
export class TodoService {

    private url: string = "http://localhost:3000/api/todo-list";

    constructor() { }

    add(todo: Todo) {
        return axios.post(this.url, todo);
    }

    getAll() {
        return axios.get(this.url);
    }

    update(todo: Todo) {
        return axios.put(`${this.url}/${todo.id}`, todo);
    }

    delete(todo: Todo) {
        return axios.delete(`${this.url}/${todo.id}`);
    }
}
