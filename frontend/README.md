# Frontend

## Install

Para instalar as dependências da aplicação, é necessário executar o comando `npm install`. Com isso todas as dependencias necessárias serão instaladas.

## Development server

Execute o comando `ng serve` para subir a aplicação em modo de desenvolvedor. 
A aplicação estará disponivel no endereço `http://localhost:4200/`.

## Build

Execute o comando `ng build` para realizar um build do projeto. Caso esse build seja para produção, adicionar a flag `--prodld`. 
Ao finalizar o processo de build, os arquivos estaram disponíveis na pasta `dist/`.

## API

Caso a API seja publicada em um servidor, será necessário alterar o arquivo `src/app/services/todo.service.ts` com a URL correta.